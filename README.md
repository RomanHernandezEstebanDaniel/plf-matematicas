# Programación Lógica y Funcional -SCC1019-ISA-20213

## Actividad: Matemáticas

### Conjuntos, Aplicaciones y funciones (2002)

```plantuml 
@startmindmap 
<style>
mindmapDiagram{
   
    BackGroundColor  	#e4fbfb
    LineBorder none
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  white
    }

}
</style>

*[#caacf9] Conjuntos, Aplicaciones y funciones
 *_ usa
  *[#f9a59a] Matemáticas 
   *_ es
    *[#f9a59a] Una ciencia que parte de una deducción lógica
   *_ características
    *[#f9a59a] Soluciona problemas de calculo
    *[#f9a59a] Utiliza el razonamiento abstracto
    *[#f9a59a] Dominio de las ideas

 *[#f9d99a] Conjuntos
  *_ se define como
   *[#f9d99a] Colección, reunión de elementos intuitivamente
  *_ usa la
   *[#f9d99a] Inclusión de conjuntos
    *_ es 
     *[#f9d99a] La primera nocion que hay entre los conjuntos con la relacion de orden que hay entre numeros
  *_ utilizan la
   *[#f9d99a] Operación de conjuntos
    *_ es
     *[#f9d99a] La construcción de conjuntos más complejos.
  *_ se dividen en
   *[#f9d99a] Universal
    *_ es
     *[#f9d99a] Donde ocurre todas las cosas con la teoría
   *[#f9d99a] Vacío
    *_ es
     *[#f9d99a] Cuando no contienen ningún elemento
  *_ utiliza la
   *[#f9d99a] Cardinal de conjuntos
    *_ es    
     *[#f9d99a] El número de elementos que conforman el conjunto
    *_ se basa en
     *[#f9d99a] La acotación de cardinales 
  *_ se representan con
   *[#f9d99a] Diagramas de Venn
 
 *[#add5fa] Aplicaciones
  *_ es
   *[#add5fa] La transformación de un elemento único
  *_ sus tipos son
   *[#add5fa] Aplicaciones inyectivas
    *_ es
     *[#add5fa] Dos elementos que tiene la misma imagen serán el mismo elemento
   *[#add5fa] Aplicaciones sobreyectiva
    *_ es
     *[#add5fa] Si son iguales
   *[#add5fa] Aplicaciones biyectiva
    *_ es   
     *[#add5fa] Cuando es inyectiva y biyectiva

 *[#f79ae5] Funciones
  *_ es
   *[#f79ae5] Cuando tenemos una transformación 
    *_ de
     *[#f79ae5] El primer elemento al segundo
  *_ utiliza la
   *[#f79ae5] Grafica de la función
    *_ es
     *[#f79ae5] Una representación grafica 
      *_ como
       *[#f79ae5] Parábolas
       *[#f79ae5] Series de puntos 
       *[#f79ae5] Líneas rectas
  @endmindmap 
  ```
#### Fuente de información:

[Conjuntos, Aplicaciones y funciones (2002)](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)


### Funciones (2010)

```plantuml 
@startmindmap 
<style>
mindmapDiagram{
   
    BackGroundColor  	#d8f8e1
    LineBorder none
    LineColor red
    node{
        LineColor none
        
        BackGroundColor  white
    }

}
</style>

*[#ededaf] Funciones
 *_ es 
  *[#c7f6d4] Un pensamiento del hombre para comprender su entorno
   *_ sirve 
    *[#c7f6d4] Para resolver problemas cotidianos con matemáticas
 *_ se definen como
  *[#fcfcda] Transformación de un conjunto a otro
 *[#e1b1bc] Tipos
  *[#e1b1bc] Crecientes 
   *_ es
    *[#e1b1bc] Cuando la variable independiente aumenta los valores del conjunto tambien aumentan
  *[#e1b1bc] Decrecientes  
   *[#e1b1bc] Cuando la variable independiente disminuye los valores del conjunto tambien disminuye

 *_ se dividen en
  *[#f6d1de] Continuas
   *_ es
    *[#f6d1de] Una función con buen comportamiento
   *_ características
    *[#f6d1de] Es una función más manejable
    *[#f6d1de] No produce discontinuidades
    *[#f6d1de] No produce saltos al graficar
  *[#f6d1de] Discontinuas
   *_ es
    *[#f6d1de] Cuando es necesario hacer saltos al graficar
   *_ características
    *[#f6d1de] Presenta discontinuidades
    *[#f6d1de] Produce saltos al graficar
 
 *[#ff6565] Características
  *[#ff6565] Limite
   *_ es
    *[#ff6565] La aproximación al punto de interés
    *[#ff6565] Continua
  *[#ff6565] Intervalo
   *_ es
    *[#ff6565] Un trozo del rango de los valores que se puede tomar
  *[#ff6565] Máximos
   *_ es
    *[#ff6565] El valor más grande que puede tomar un punto
  *[#ff6565] Mínimos
   *_ es 
    *[#ff6565] El valor más pequeño que puede tomar un punto 
  *[#ff6565] Dominio
   *_ es
    *[#ff6565] El conjunto de elementos de la variable independiente
  *[#ff6565] Derivada
   *_ es
    *[#ff6565] La aproximación local de una función
   *[#ff6565] se usa
    *[#ff6565] Para resolver problemas de aproximación

 *_ utiliza
  *[#a788ab] El plano cartesiano
   *_ es 
    *[#a788ab] Una forma de ubicar puntos en el espacio
   *_ se utiliza para 
    *[#a788ab] Representar funciones

  @endmindmap 
  ```
#### Fuente de información:

[Funciones (2010)](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)  

### La matemática del computador (2002)

```plantuml 
@startmindmap 
<style>
mindmapDiagram{
   
    BackGroundColor  	#ff9688
    LineBorder black
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  white
    }

}
</style>

*[#6a9eda] La matemática del computador
 *[#e3b1c8] Matemáticas
  *_ es
   *[#e3b1c8] Una ciencia que parte de una deducción lógica 
  *_ utiliza la 
   *[#e3b1c8] Aritmética finita
    *_ características
     *[#e3b1c8] Truncar un numero
      *_ es
       *[#e3b1c8] Cortar los decimales de un numero
      *_ sirve para
       *[#e3b1c8] Cortar un número que tiene una infinidad de decimales
        *_ por ejemplo
         *[#e3b1c8] π=3.1416.....
         *[#e3b1c8] e=2.7182.....
     *[#e3b1c8] Redondear un numero
      *_ es 
       *[#e3b1c8] El truncamiento refinado y retocar la última cifra
      *_ sirve para
       *[#e3b1c8] Para que no haya errores  
     *[#e3b1c8] Dígitos significativos
      *_ sirve para
       *[#e3b1c8] Medir precisión general relativa de un valor 
    *_ utiliza la  
     *[#e3b1c8] Materialización de un abstracto
      *_ ejemplos
       *[#e3b1c8] Números
        *_ sirve para
         *[#e3b1c8] Operaciones matemáticas
       *[#e3b1c8] Letras
        *_ sirve para
         *[#e3b1c8] Los textos
       *[#e3b1c8] Signos
        *_ sirve para
         *[#e3b1c8] La dualidad de valores
       *[#e3b1c8] Instrucciones
        *_ sirve para
         *[#e3b1c8] Creación de algoritmos 

 *[#c8ca66] Sistemas de números
  *_ es
   *[#c8ca66] Conjunto de símbolos y reglas de generación que permiten construir todos los números válidos
  *_ permite
   *[#c8ca66] El conteo de los elementos de un conjunto
  *_ se define por
   *[#c8ca66] La base que utiliza 
  *[#c8ca66] Tipos 
   *[#c8ca66] Sistema binario
    *_ características
     *[#c8ca66] Enlaza la lógica binaria
     *[#c8ca66] Representa un numero mediante bits o dígitos binarios
     *[#c8ca66] Lógica de dos ordenadores
     *[#c8ca66] Creación de estructuras complejas
    *_ se aplica en
     *[#c8ca66] Circuitos eléctricos 
   *[#c8ca66] Sistema octal
    *_ características
     *[#c8ca66] Sistema de numeración de base 8
     *[#c8ca66] Se utiliza en la informática
     *[#c8ca66] No utiliza símbolos 
    *_ se aplica 
     *[#c8ca66] Como una forma abreviada de representar números binarios que emplean caracteres de seis bits

   *[#c8ca66] Sistema hexadecimal
    *_ características
     *[#c8ca66] Sistema de numeración de base 16
     *[#c8ca66] se basa en el uso de símbolos, potencias y posiciones
    *_ se aplica 
     *[#c8ca66] Cuando se trabaja con computadores
      *_ para
       *[#c8ca66] Representar números binarios de manera más legible

  @endmindmap 
  ```

#### Fuentes de información:

[La matemática del computador (2002)](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
[Las matemáticas de los computadores](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)

Autor:
[RomanHernandezEstebanDaniel @ GitLab](https://gitlab.com/RomanHernandezEstebanDaniel)

